
### angular-cli

Complete image to dev and build an Angular 4 application with Angular-cli.
Image also usable in Jenkins or BitBucket pipelines
Contains :

- Google Chrome for testing
- Sonar Scanner for quality checks integration in Continuous integration

**Versions**

- Node 8.2.1
- NPM 5.3.0
- Angular-Cli v1.2.7
- Oracle-jdk 8
- Sonar Scanner 3.0.0.702-linux
- Google Chrome 60.0.3112.90

